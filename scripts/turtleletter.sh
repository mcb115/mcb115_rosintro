#!/bin/bash

rosservice call /reset

# lift the pen on the turtle
rosservice call turtle1/set_pen 0 0 0 0 1

# move the turtle to the starting point
rosservice call /turtle1/teleport_absolute 1.5 1.5 1.308

# put the pen down
rosservice call /turtle1/set_pen 255 255 255 2 0

# draw the letter M

# draw the first line
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[8,0 ,0]' '[0, 0, 0]'

# turn the turtle
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,0,0]' '[0, 0, -2.616]'

# draw the second line
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[8,0 ,0]' '[0, 0, 0]'

# turn the turtle
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,0,0]' '[0, 0, 2.616]'

# draw the third line
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[8,0 ,0]' '[0, 0, 0]'

# turn the turtle
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,0,0]' '[0, 0, -2.616]'

# draw the fourth line
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[8,0 ,0]' '[0, 0, 0]'
