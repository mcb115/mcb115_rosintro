from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import tau, radians
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from matplotlib import pyplot as plt
import numpy as np


# Initialize the moveit_commander and rospy nodes
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

# Instantiate a RobotCommander object. This object is an interface to the robot as a whole.
robot = moveit_commander.RobotCommander()

# Instantiate a PlanningSceneInterface object. This object is an interface to the world surrounding the robot.
scene = moveit_commander.PlanningSceneInterface()

# Load UR5e as the robot model
group_name = 'manipulator'
move_group = moveit_commander.MoveGroupCommander(group_name)

# Create a DisplayTrajectory ROS publisher which is used to display trajectories in Rviz
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)


def start_sequence():
    """Moves the robot to the start position"""

    # Set the robot back to the home position before starting the script
    move_group.set_named_target("home")
    plan_home = move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()

    # Get robot in "ready" position
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] = 3*tau/8 #Slew
    joint_goal[1] = -tau / 4 #Shoulder
    joint_goal[2] = tau/4 #Elbow
    joint_goal[3] = -tau/4 #Wrist
    joint_goal[4] = -tau/4 #Wrist Rotation
    joint_goal[5] = tau / 6 #Gripper

    # The go command can be called with joint values, poses, or without any
    # parameters if you have already set the pose or joint target for the group
    move_group.go(joint_goal, wait=True)

    # Calling ``stop()`` ensures that there is no residual movement
    move_group.stop()

    # Print the current pose of the end-effector to the terminal
    current_pose = move_group.get_current_pose().pose
    print("Starting pose:", current_pose)


def route_cartesian(waypoints: list):
    """Moves the robot along a path defined by the waypoints"""
    plan, fraction = move_group.compute_cartesian_path(
                        waypoints,  # waypoints to follow
                        0.01,  # eef_step
                        0.0,  # jump_threshold
                     )

    # Note: We are just planning, not asking move_group to actually move the robot yet:
    print("Fraction:", fraction)
    print("Plan:", plan)

    # Move the robot
    move_group.execute(plan, wait=True)


def rot_mat(ang_rad) -> np.ndarray:
    """Returns a 3x3 rotation matrix for a rotation about the z-axis"""
    return np.array([
        [np.cos(ang_rad), -np.sin(ang_rad), 0],
        [np.sin(ang_rad), np.cos(ang_rad), 0],
        [0, 0, 1]
    ])

def gen_waypoints(points: np.ndarray) -> list:
    """Returns a list of waypoints for the robot to follow"""
    waypoints = []
    for point in points:
        # Copy the current pose of the end-effector
        waypoints.append(copy.deepcopy(move_group.get_current_pose().pose))

        # Move the end-effector to the next point
        waypoints[-1].position.x = point[0]
        waypoints[-1].position.y = point[1]
        given_z = point[2]
        waypoints[-1].position.z = given_z if given_z > 0.05 else 0.2 # Error checking for the pen rammimg into the ground

    return waypoints

def pick_up_pen():
    """Moves the robot to the pick up pen position"""
    # Get the current pose of the end-effector
    current_pose = move_group.get_current_pose().pose

    # Copy the current pose of the end-effector
    waypoints = []
    waypoints.append(copy.deepcopy(current_pose))

    # Move the end-effector upto z=0.2m
    waypoints[-1].position.z = 0.2

    # Move the robot along the path
    route_cartesian(waypoints)


def drop_pen():
    """Moves the robot to the drop pen position"""
    # Get the current pose of the end-effector
    current_pose = move_group.get_current_pose().pose

    # Copy the current pose of the end-effector
    waypoints = []
    waypoints.append(copy.deepcopy(current_pose))

    # Move the end-effector down in z to 0.2m
    waypoints[-1].position.z = 0.1

    # Move the robot along the path
    route_cartesian(waypoints)


def move_robot_along_path(points: list):
    """Moves the robot along a path defined by the points. Picks up and drops the pen at the start and end of the path"""
    # Create a list of waypoints marking out the letters
    waypoints_list = [gen_waypoints(points_i) for points_i in points]

    # Pick up the pen
    pick_up_pen()

    # Move the robot along the paths
    for waypoints in waypoints_list:

        # Route to above the start of the path
        route_cartesian([waypoints[0]])

        # Drop the pen
        drop_pen()

        # Move the robot along the path
        route_cartesian(waypoints)

        # Pick up the pen
        pick_up_pen()


# Experimentally, we know that the maximum range of the UR5e is 0.85m
# Therefore, we will plot our path at a maximum distance of 0.85m from the origin
# The approach will be to calculate the points for the letter as if it were righ in front of the robot and then rotate the M to the left and the B to the right to form a MCB that wraps around the robot

# Create a figure and a grid layout with 2 rows and 3 columns
fig = plt.figure(figsize=(10, 8))
grid = plt.GridSpec(2, 3, height_ratios=[1, 2])

# Used to rotate all the letters at once (degrees)
tie_rot = -8

# The verticies of an M, placed in front of the robot
width_m = 0.4
height_m = 0.4
vert_offset_m = 0.35
m_points = np.array([
    [-width_m/2, vert_offset_m, 0],
    [-width_m/2, height_m + vert_offset_m, 0],
    [0, height_m/2 + vert_offset_m, 0],
    [width_m/2, height_m + vert_offset_m, 0],
    [width_m/2, vert_offset_m, 0]
])

# Rotate M
R_M = rot_mat(radians(-54 + tie_rot))

# Rotate M
m_rot = np.matmul(m_points, R_M)

# The verticies of a C, placed in front of the robot
width_c = 0.4
height_c = 0.45
vert_offset_c = 0.55
min_angle_c = radians(45) # Where the line of the C starts
max_angle_c = radians(315) # Where the line of the C ends
t = np.linspace(min_angle_c, max_angle_c, 20)
# c_points is an ellipse with a major axis of width_c and a minor axis of height_c
c_points = np.array([
    [width_c/2 * np.cos(theta), height_c/2 * np.sin(theta) + vert_offset_c, 0]
    for theta in t
])

# Rotate C (this is becuase the M is wide and I am centering the MCB as a whole)
R_C = rot_mat(radians(10 + tie_rot))

# Rotate C
c_rot = np.matmul(c_points, R_C)

# The verticies of a B, placed in front of the robot
width_b = 0.45
height_b = 0.45
vert_offset_b = 0.33
angle_b = 90
min_angle_b = radians(angle_b) # Where the line of the B starts
max_angle_b = radians(-angle_b) # Where the line of the B ends
top_arc_scale_width_b = 0.8 # Width of the top arc relative to the bottom arc
# First add the left line of the B
b_points = np.array([
    [-width_b/4, vert_offset_b, 0],
    [-width_b/4, height_b + vert_offset_b, 0]
])

# Now find the top and bottom curves of the B
# Both arcs are ellipses with a major axis of width_b and a minor axis of height_b
t = np.linspace(min_angle_b, max_angle_b, 15)
width_b *= top_arc_scale_width_b
top_arc_b = np.array([
    [width_b/2 * np.cos(theta), height_b/4 * np.sin(theta) + 0.75 * height_b + vert_offset_b, 0]
    for theta in t
])
width_b /= top_arc_scale_width_b
bottom_arc_b = np.array([
    [width_b/2 * np.cos(theta), height_b/4 * np.sin(theta) + 0.25 * height_b + vert_offset_b, 0]
    for theta in t
])

# To align the arcs with the line, move them the difference in x between the end of the line and the start of the arc
diff_x_b = b_points[-1, 0] - top_arc_b[0, 0]
top_arc_b[:, 0] += diff_x_b
bottom_arc_b[:, 0] += diff_x_b

# Combine the points
b_points_combined = np.concatenate((b_points, top_arc_b, bottom_arc_b))

# Rotate B
R_B = rot_mat(radians(64 + tie_rot))

# Rotate B
b_rot = np.matmul(b_points_combined, R_B)

# Rotate the individual segments of B
b_rot_top = np.matmul(top_arc_b, R_B)
b_rot_bottom = np.matmul(bottom_arc_b, R_B)
b_rot_left = np.matmul(b_points, R_B)

# Plot the unrotated M
ax1 = plt.subplot(grid[0, 0])
ax1.plot(m_points[:, 0], m_points[:, 1], 'k')
# Set axes labels
ax1.set_xlabel('x (m)')
ax1.set_ylabel('y (m)')
# Equal aspect ratio
ax1.set_aspect('equal', 'box')

# Plot the rotated C
ax2 = plt.subplot(grid[0, 1])
ax2.plot(c_points[:, 0], c_points[:, 1], 'k')
# Set axes labels
ax2.set_xlabel('x (m)')
ax2.set_ylabel('y (m)')
# Equal aspect ratio
ax2.set_aspect('equal', 'box')

# Plot the unrotated B
ax3 = plt.subplot(grid[0, 2])
ax3.plot(b_points_combined[:, 0], b_points_combined[:, 1], 'k')
# Set axes labels
ax3.set_xlabel('x (m)')
ax3.set_ylabel('y (m)')
# Equal aspect ratio
ax3.set_aspect('equal', 'box')

# Plot the combined MCB

# Plot the boundary circle and the three letters (with rotation)
ax4 = plt.subplot(grid[1, :])

# Plot a circle of radius 0.85m
circle = plt.Circle((0, 0), 0.85, color='k', fill=False)
ax4.add_artist(circle)

# Plot the rotated M
ax4.plot(m_rot[:, 0], m_rot[:, 1], 'r', label='M')

# Plot the unrotated C
ax4.plot(c_rot[:, 0], c_rot[:, 1], 'g', label='C')

# Plot the rotated B
ax4.plot(b_rot[:, 0], b_rot[:, 1], 'b', label='B')

# Fit to the grid
ax4.set_aspect('equal', 'box')

# Set axes limits to +/- 0.9
ax4.set_xlim([-0.9, 0.9])
ax4.set_ylim([-0.9, 0.9])

ax4.legend()

# Adjust spacing between subplots
plt.tight_layout()

# Show the figure but do not block the script
plt.show()

# For the sake of the robot not hitting the ground, we will add a z offset of 0.1m to the points
m_rot[:, 2] += 0.1
c_rot[:, 2] += 0.1
b_rot_top[:, 2] += 0.1
b_rot_bottom[:, 2] += 0.1
b_rot_left[:, 2] += 0.1

# Start the robot in the "ready" position -- this avoids the singularity at the home position
start_sequence()

route = [m_rot, c_rot, b_rot_left, b_rot_top, b_rot_bottom]


# Move the robot
move_robot_along_path(route)

print("Route:", route)