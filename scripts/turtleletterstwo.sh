#!/bin/bash

rosservice call /reset

# lift the pen on the turtle
rosservice call turtle1/set_pen 0 0 0 0 1

# move the turtle to the starting point
rosservice call /turtle1/teleport_absolute 0.5 1.5 1.308

# put the pen down
rosservice call /turtle1/set_pen 255 255 255 2 0

# draw the letter M

# draw the first line
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[4,0 ,0]' '[0, 0, 0]'

# turn the turtle
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,0,0]' '[0, 0, -2.616]'

# draw the second line
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[4,0 ,0]' '[0, 0, 0]'

# turn the turtle
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,0,0]' '[0, 0, 2.616]'

# draw the third line
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[4,0 ,0]' '[0, 0, 0]'

# turn the turtle
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,0,0]' '[0, 0, -2.616]'

# draw the fourth line
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[4,0 ,0]' '[0, 0, 0]'

# raise the pen
rosservice call /turtle1/set_pen 0 0 0 0 1

# move the turtle to the second letter
rosservice call /turtle1/teleport_absolute 6 1.5 0

# put the pen down in a different color
rosservice call /turtle1/set_pen 25 55 150 2 0

# draw the letter I

# move direct right to form the base of the letter I
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[4,0 ,0]' '[0, 0, 0]'

# turn back on itself
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,0,0]' '[0, 0, 3.1416]'

# move back half way along the base
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[2,0 ,0]' '[0, 0, 0]'

# turn up
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,0,0]' '[0, 0, -1.5708]'

# now move up to form the vertical part of the letter I
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[4,0 ,0]' '[0, 0, 0]'

# turn left
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,0,0]' '[0, 0, 1.5708]'

# move left to form the top of the letter I
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[2,0 ,0]' '[0, 0, 0]'

# turn back on itself
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[0,0,0]' '[0, 0, 3.1416]'

# move back along the top
rostopic pub -1 turtle1/cmd_vel geometry_msgs/Twist -- '[4,0 ,0]' '[0, 0, 0]'
