from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import tau
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list


# Initialize the moveit_commander and rospy nodes
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

# Instantiate a RobotCommander object. This object is an interface to the robot as a whole.
robot = moveit_commander.RobotCommander()

# Instantiate a PlanningSceneInterface object. This object is an interface to the world surrounding the robot.
scene = moveit_commander.PlanningSceneInterface()

# Load UR5e as the robot model
group_name = 'manipulator'
move_group = moveit_commander.MoveGroupCommander(group_name)

# Create a DisplayTrajectory ROS publisher which is used to display trajectories in Rviz
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# Set the robot back to the home position before starting the script
move_group.set_named_target("home")
plan_home = move_group.go(wait=True)
move_group.stop()
move_group.clear_pose_targets()


def start_sequence():
    """Moves the robot to the start position"""

    # We get the joint values from the group and change some of the values:
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] = 0 #Slew
    joint_goal[1] = -tau / 8 #Shoulder
    joint_goal[2] = 0 #Elbow
    joint_goal[3] = -tau / 4 #Wrist
    joint_goal[4] = 0 #Wrist Rotation
    joint_goal[5] = tau / 6 #Gripper

    # The go command can be called with joint values, poses, or without any
    # parameters if you have already set the pose or joint target for the group
    move_group.go(joint_goal, wait=True)

    # Calling ``stop()`` ensures that there is no residual movement
    move_group.stop()

    # Get robot in "ready" position
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] = 0 #Slew
    joint_goal[1] = -tau / 4 #Shoulder
    joint_goal[2] = tau/4 #Elbow
    joint_goal[3] = -tau/4 #Wrist
    joint_goal[4] = -tau/4 #Wrist Rotation
    joint_goal[5] = tau / 6 #Gripper

    # The go command can be called with joint values, poses, or without any
    # parameters if you have already set the pose or joint target for the group
    move_group.go(joint_goal, wait=True)

    # Calling ``stop()`` ensures that there is no residual movement
    move_group.stop()

    # Print the current pose of the end-effector to the terminal
    current_pose = move_group.get_current_pose().pose
    print("Starting pose:", current_pose)

def route_pose_goal(goal):
    """Moves the robot to the goal position"""

    move_group.set_pose_target(goal)

    # Call the planner to compute the plan and execute it.
    plan = move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()

    # Print the current pose of the end-effector to the terminal
    current_pose = move_group.get_current_pose().pose
    print("Current pose:", current_pose)


# Start the robot in the "ready" position -- this avoids the singularity at the home position
start_sequence()

# Move to the bottom left corner of the M
pose_goal = geometry_msgs.msg.Pose()
pose_goal.position.x = 0.1
pose_goal.position.y = 0.6
pose_goal.position.z = 0.1
pose_goal.orientation.x = -1.0
pose_goal.orientation.y = 0
pose_goal.orientation.z = 0
pose_goal.orientation.w = 0
route_pose_goal(pose_goal)

# Move to the top left corner of the M
pose_goal = geometry_msgs.msg.Pose()
pose_goal.position.x = 0.5
pose_goal.position.y = 0.6
pose_goal.position.z = 0.1
pose_goal.orientation.x = -1.0
pose_goal.orientation.y = 0
pose_goal.orientation.z = 0
pose_goal.orientation.w = 0
route_pose_goal(pose_goal)

# Move diagonally down
pose_goal = geometry_msgs.msg.Pose()
pose_goal.position.x = 0.3
pose_goal.position.y = 0.5
pose_goal.position.z = 0.1
pose_goal.orientation.x = -1.0
pose_goal.orientation.y = 0
pose_goal.orientation.z = 0
pose_goal.orientation.w = 0
route_pose_goal(pose_goal)

# Move diagonally up
pose_goal = geometry_msgs.msg.Pose()
pose_goal.position.x = 0.5
pose_goal.position.y = 0.4
pose_goal.position.z = 0.1
pose_goal.orientation.x = -1.0
pose_goal.orientation.y = 0
pose_goal.orientation.z = 0
pose_goal.orientation.w = 0
route_pose_goal(pose_goal)

# Move down right side of M
# Move diagonally up
pose_goal = geometry_msgs.msg.Pose()
pose_goal.position.x = 0.1
pose_goal.position.y = 0.4
pose_goal.position.z = 0.1
pose_goal.orientation.x = -1.0
pose_goal.orientation.y = 0
pose_goal.orientation.z = 0
pose_goal.orientation.w = 0
route_pose_goal(pose_goal)




