#!/bin/bash

tmux kill-server

session="initial_m"

tmux new-session -d -s $session

tmux split-window -h -p 50
tmux split-window -v -p 50

tmux select-pane -t 0

tmux send-keys "roslaunch ur_gazebo ur5_bringup.launch" C-m

tmux select-pane -t 1
tmux send-keys "roslaunch ur5_moveit_config moveit_planning_execution.launch sim:=true" C-m

tmux select-pane -t 2
tmux send-keys "python3 initial_m.py" C-m

tmux attach-session -t $session